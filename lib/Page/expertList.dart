import 'package:flutter/material.dart';
import 'package:flutter_app/Design/Skeleton/sk_skeleton.dart';
import 'package:flutter_app/Design/profileCard.dart';
import 'package:flutter_app/Helper/Color_me.dart';
import 'package:flutter_app/Helper/commonWidget.dart';
import 'package:flutter_app/PROVIDER/get_data.dart';
import 'package:flutter_app/Responsive/responsive_ui.dart';
import 'package:provider/provider.dart';

class ExpertList extends StatefulWidget {
  @override
  _ExpertListState createState() => _ExpertListState();
}

class _ExpertListState extends State<ExpertList> {
  double _height, _width;
  double _pixelRatio;
  bool large;
  bool medium;

  final List texColor = [
    Color_me.blue,
    Color_me.green,
    Color_me.red,
    Color_me.purple,
    Color_me.potato,
    Color_me.green,
    Color_me.red,
    Color_me.purple,
    Color_me.p_red,
    Color_me.green,
    Color_me.red,
    Color_me.purple,
    Color_me.dark_blue,
    Color_me.green,
    Color_me.red,
    Color_me.purple,
    Color_me.dark_blue,
    Color_me.green,
    Color_me.red,
    Color_me.purple,
    Color_me.blue,
    Color_me.green,
    Color_me.red,
    Color_me.purple,
    Color_me.potato,
    Color_me.green,
    Color_me.red,
    Color_me.purple,
    Color_me.p_red,
    Color_me.green,
    Color_me.red,
    Color_me.purple,
    Color_me.dark_blue,
    Color_me.green,
    Color_me.red,
    Color_me.purple,
    Color_me.dark_blue,
    Color_me.green,
    Color_me.red,
    Color_me.purple,
  ];

  @override
  void initState() {
    var fetch_popular = Provider.of<GetData>(context, listen: false);
    fetch_popular.get_userData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);
    return SafeArea(child: Scaffold(body: Consumer<GetData>(
      builder: (content, data, child) {
        return Container(
          height: _height,
          width: _width,
          child: data.userInfo != null
              ? ListView.builder(
                  itemCount: data.userInfo.data.length,
                  itemBuilder: (context, index) {
                    return Padding(
                        padding: EdgeInsets.all(5),
                        child: ProfileCard(
                          profileImage: data
                              .userInfo.data[index].profile.avatarUrl
                              .toString(),
                          avtrImage: data
                              .userInfo.data[index].profile.coverPhotoUrl
                              .toString(),
                          name:
                              "${data.userInfo.data[index].firstName} ${data.userInfo.data[index].lastName}",
                          qualification: data
                              .userInfo.data[index].profile.qualification
                              .toString(),
                          countryName: data.userInfo.data[index].profile.country
                              .toString(),
                          skill: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: data
                                  .userInfo.data[index].profile.skills.length,
                              itemBuilder: (context, i) {
                                return Padding(
                                  padding: EdgeInsets.only(
                                      left: 5, right: 5, top: 5),
                                  child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(10.0),
                                            bottomRight: Radius.circular(10.0),
                                            topLeft: Radius.circular(10.0),
                                            bottomLeft: Radius.circular(10.0),
                                          ),
                                          color: Color_me.main3),
                                      child: Center(
                                        child: setCommonText(
                                            " #${data.userInfo.data[index].profile.skills[i]} ",
                                            texColor[i],
                                            15.0,
                                            FontWeight.normal,
                                            1),
                                      )),
                                );
                              }),
                        ));
                  })
              : Center(
                  child: SkCategory(),
                ),
        );
      },
    )));
  }
}
