import 'package:flutter/material.dart';
import 'package:flutter_app/Helper/Color_me.dart';
import 'package:flutter_app/Page/expertList.dart';
import 'package:flutter_app/Responsive/responsive_ui.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  double _height, _width;
  double _pixelRatio;
  bool large;
  bool medium;
  int _selectedIndex = 0;
  PageController _pageController = PageController();

  void _onPageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _onItemTaped(int selectedIndex) {
    setState(() {
      _pageController.jumpToPage(selectedIndex);
    });
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: Color_me.dark_blue,
        automaticallyImplyLeading: false,

        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ), // Don't show the leading button
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(Icons.arrow_back_ios, color: Colors.white),
            ),
            Expanded(
                child: Text(
              "Expert Listing",
              textAlign: TextAlign.center,
            ))
            // Your widgets here
          ],
        ),
      ),
      body: Container(
        height: _height,
        width: _width,
        child: ExpertList(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 10.0,
        type: BottomNavigationBarType.fixed,
        /*backgroundColor: Color_me.main,*/
        showUnselectedLabels: true,
        unselectedItemColor: Colors.black,
        selectedItemColor: Color_me.main,
        onTap: _onItemTaped,
        currentIndex: _selectedIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: _selectedIndex == 0 ? Color_me.main : Colors.grey,
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.zoom_in_outlined,
                color: _selectedIndex == 1 ? Color_me.main : Colors.grey,
              ),
              label: "Browse"),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.settings,
                color: _selectedIndex == 2 ? Color_me.main : Colors.grey,
              ),
              label: "Settings"),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.dashboard,
              color: _selectedIndex == 3 ? Color_me.main : Colors.grey,
            ),
            label: "Dashboard",
          ),
        ],
      ),
    ));
  }
}
