import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Helper/route.dart';
import 'package:flutter_app/PROVIDER/get_data.dart';
import 'package:flutter_app/Page/homepage.dart';
import 'package:flutter_app/Responsive/size_config.dart';
import 'package:flutter_app/Slash/splashscreen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
      ChangeNotifierProvider(create: (context) => GetData(), child: MyApp()));
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              builder: (context, child) {
                return ScrollConfiguration(
                  behavior: MyBehavior(),
                  child: child,
                );
              },
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                /*cursorColor: Color_me.green,*/
                visualDensity: VisualDensity.adaptivePlatformDensity,
                errorColor: Colors.red,
              ),
              /*.copyWith(
                pageTransitionsTheme: const PageTransitionsTheme(
                  builders: <TargetPlatform, PageTransitionsBuilder>{
                    TargetPlatform.android: ZoomPageTransitionsBuilder(),
                  },
                ),
              ),*/
              home: Home(),
              routes: <String, WidgetBuilder>{
                HOME: (BuildContext context) => Home(),
                SPLASH_SCREEN: (BuildContext context) => SplashScreen(),
              },
              initialRoute: SPLASH_SCREEN,
            );
          },
        );
      },
    );
  }
}
