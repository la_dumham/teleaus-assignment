import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';

setCommonText(String title, dynamic color, dynamic fontSize, dynamic fontweight,
    dynamic noOfLine,
    {var talign}) {
  return new AutoSizeText(
    title,
    textAlign: talign,
    textDirection: TextDirection.ltr,
    style: new TextStyle(
      color: color,
      fontSize: fontSize,
      fontWeight: fontweight,
    ),
    maxLines: noOfLine,
    overflow: TextOverflow.ellipsis,
    wrapWords: true,
  );
}
