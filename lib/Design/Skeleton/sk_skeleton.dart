import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Responsive/responsive_ui.dart';
import 'package:skeleton_text/skeleton_text.dart';

class SkCategory extends StatefulWidget {
  @override
  _SkCategoryState createState() => _SkCategoryState();
}

class _SkCategoryState extends State<SkCategory> {
  double _height, _width;
  double _pixelRatio;
  bool large;
  bool medium;

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);
    return SingleChildScrollView(
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
          SkeletonWidget(),
        ],
      ),
    );
  }

  Widget SkeletonWidget() {
    return Padding(
      padding: EdgeInsets.all(5),
      child: SkeletonAnimation(
          child: Container(
        padding: EdgeInsets.all(10),
        width: _width,
        child: Column(
          children: [
            Container(
              width: _width,
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 20.0,
                    backgroundColor: Colors.black12,
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: 10, bottom: 10),
                      child: Column(
                        children: [
                          Container(
                            width: _width,
                            child: Container(
                              width: _width / 3,
                              height: 10,
                              padding: EdgeInsets.all(5),
                              color: Colors.black12,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 5),
                            child: Container(
                              width: _width,
                              child: Row(
                                children: [
                                  CircleAvatar(
                                    radius: 7.0,
                                    backgroundColor: Colors.black12,
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Container(
                                        color: Colors.black12,
                                        width: _width / 2,
                                        padding: EdgeInsets.all(5),
                                        height: 6,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: _width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Container(
                        padding: EdgeInsets.all(5),
                        child: Container(
                          height: 8,
                          color: Colors.black12,
                          width: _width / 2,
                        )),
                  ),
                  Container(
                    color: Colors.black12,
                    width: _width / 1.5,
                    height: 5,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Container(
                width: _width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      color: Colors.black12,
                      height: 5,
                      width: _width / 1.5,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
