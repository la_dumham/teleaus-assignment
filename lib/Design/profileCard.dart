import 'package:flutter/material.dart';
import 'package:flutter_app/Helper/commonWidget.dart';
import 'package:flutter_app/Responsive/responsive_ui.dart';

class ProfileCard extends StatefulWidget {
  final String name;
  final String profileImage;
  final String avtrImage;
  final String countryName;
  final String qualification;
  final Widget skill;

  const ProfileCard(
      {Key key,
      this.name,
      this.profileImage,
      this.avtrImage,
      this.countryName,
      this.qualification,
      this.skill})
      : super(key: key);

  @override
  _ProfileCardState createState() => _ProfileCardState();
}

class _ProfileCardState extends State<ProfileCard> {
  double _height, _width;
  double _pixelRatio;
  bool large;
  bool medium;

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);

    return Container(
      padding: EdgeInsets.all(10),
      width: _width,
      child: Column(
        children: [
          Container(
            width: _width,
            child: Row(
              children: [
                CircleAvatar(
                  radius: 20.0,
                  backgroundImage: NetworkImage(widget.profileImage),
                  backgroundColor: Colors.transparent,
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      children: [
                        Container(
                          width: _width,
                          child: Container(
                              padding: EdgeInsets.all(5),
                              child: setCommonText(widget.name, Colors.black,
                                  16.0, FontWeight.w500, 1)),
                        ),
                        Container(
                          width: _width,
                          child: Row(
                            children: [
                              CircleAvatar(
                                radius: 7.0,
                                backgroundImage: NetworkImage(widget.avtrImage),
                                backgroundColor: Colors.transparent,
                              ),
                              Expanded(
                                child: Container(
                                    padding: EdgeInsets.all(5),
                                    child: setCommonText(
                                        widget.countryName,
                                        Colors.black,
                                        12.0,
                                        FontWeight.normal,
                                        1)),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            width: _width,
            child: Column(
              children: [
                Container(
                  width: _width,
                  child: Container(
                      padding: EdgeInsets.all(5),
                      child: setCommonText("About The Expert", Colors.black,
                          15.0, FontWeight.w500, 1)),
                ),
                Container(
                  width: _width,
                  child: Container(
                      padding: EdgeInsets.only(left: 5),
                      child: setCommonText(widget.qualification, Colors.black,
                          15.0, FontWeight.w300, 1)),
                ),
              ],
            ),
          ),
          Container(
            width: _width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [Container(height: 25, child: widget.skill)],
            ),
          )
        ],
      ),
    );
  }
}
