class UserInfo {
  bool success;
  List<Data> data;
  Meta meta;

  UserInfo({this.success, this.data, this.meta});

  UserInfo.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    return data;
  }
}

class Data {
  int id;
  String firstName;
  String lastName;
  String email;
  String username;
  String type;
  Profile profile;
  String verifiedAt;
  String createdAt;
  String updatedAt;

  Data(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.username,
      this.type,
      this.profile,
      this.verifiedAt,
      this.createdAt,
      this.updatedAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    username = json['username'];
    type = json['type'];
    profile =
        json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
    verifiedAt = json['verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['username'] = this.username;
    data['type'] = this.type;
    if (this.profile != null) {
      data['profile'] = this.profile.toJson();
    }
    data['verified_at'] = this.verifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Profile {
  int id;
  int isPublic;
  String avatarUrl;
  String coverPhotoUrl;
  String dateOfBirth;
  String phone;
  String qualification;
  String experience;
  List<String> skills;
  String businessNumber;
  String coverLetter;
  String jobTitle;
  String companyName;
  String companySize;
  String businessType;
  String companyRegistrationNumber;
  String location;
  String country;
  String createdAt;
  String updatedAt;

  Profile(
      {this.id,
      this.isPublic,
      this.avatarUrl,
      this.coverPhotoUrl,
      this.dateOfBirth,
      this.phone,
      this.qualification,
      this.experience,
      this.skills,
      this.businessNumber,
      this.coverLetter,
      this.jobTitle,
      this.companyName,
      this.companySize,
      this.businessType,
      this.companyRegistrationNumber,
      this.location,
      this.country,
      this.createdAt,
      this.updatedAt});

  Profile.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    isPublic = json['is_public'];
    avatarUrl = json['avatar_url'];
    coverPhotoUrl = json['cover_photo_url'];
    dateOfBirth = json['date_of_birth'];
    phone = json['phone'];
    qualification = json['qualification'];
    experience = json['experience'];
    skills = json['skills'].cast<String>();
    businessNumber = json['business_number'];
    coverLetter = json['cover_letter'];
    jobTitle = json['job_title'];
    companyName = json['company_name'];
    companySize = json['company_size'];
    businessType = json['business_type'];
    companyRegistrationNumber = json['company_registration_number'];
    location = json['location'];
    country = json['country'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['is_public'] = this.isPublic;
    data['avatar_url'] = this.avatarUrl;
    data['cover_photo_url'] = this.coverPhotoUrl;
    data['date_of_birth'] = this.dateOfBirth;
    data['phone'] = this.phone;
    data['qualification'] = this.qualification;
    data['experience'] = this.experience;
    data['skills'] = this.skills;
    data['business_number'] = this.businessNumber;
    data['cover_letter'] = this.coverLetter;
    data['job_title'] = this.jobTitle;
    data['company_name'] = this.companyName;
    data['company_size'] = this.companySize;
    data['business_type'] = this.businessType;
    data['company_registration_number'] = this.companyRegistrationNumber;
    data['location'] = this.location;
    data['country'] = this.country;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Meta {
  int lastPage;
  String nextPageUrl;
  String prevPageUrl;
  int total;

  Meta({this.lastPage, this.nextPageUrl, this.prevPageUrl, this.total});

  Meta.fromJson(Map<String, dynamic> json) {
    lastPage = json['last_page'];
    nextPageUrl = json['next_page_url'];
    prevPageUrl = json['prev_page_url'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['last_page'] = this.lastPage;
    data['next_page_url'] = this.nextPageUrl;
    data['prev_page_url'] = this.prevPageUrl;
    data['total'] = this.total;
    return data;
  }
}
