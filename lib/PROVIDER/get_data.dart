import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Helper/url.dart';
import 'package:flutter_app/Model/userInfo.dart';
import 'package:http/http.dart' as http;

class GetData extends ChangeNotifier {
  UserInfo userInfo;

  Future<List> get_userData() async {
    final response = await http.get(url);
    var data = json.decode(response.body.toString());

    print(data);
    userInfo = UserInfo.fromJson(data);

    notifyListeners();
  }
}
